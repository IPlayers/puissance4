package fr.epsi.puissancek.controller;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.SwingUtilities;

import fr.epsi.puissancek.gui.GamePanel;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.GameMove;
import fr.epsi.puissancek.model.GameMoveList;
import fr.epsi.puissancek.model.PlayerColor;
import fr.epsi.puissancek.model.PlayerType;
import fr.epsi.puissancek.players.Player;

public class GameController {

    private final GameModel model;
    private final GameMoveList moveList;
    private GamePanel gamePanel;
    
    private Player nextPlayer;
    
    private Player redPlayer;
    private Player yellowPlayer;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();
    
    /**
     * Create a game controller for the given game model.
     * 
     * @param model Game model (= board) to play with
     * @param redPlayer Red player type
     * @param yellowPlayer Yellow player type
     */
    public GameController(GameModel model, PlayerType redPlayer, PlayerType yellowPlayer) {
        this.model = model;
        this.moveList = new GameMoveList();
        
        this.redPlayer = Player.build(PlayerColor.RED, redPlayer, this);
        this.yellowPlayer = Player.build(PlayerColor.YELLOW, yellowPlayer, this);
        setNextPlayer(this.redPlayer);
    }
    
    /**
     * Get the color of the next player to play.
     * 
     * @return Color of the next player
     */
    public PlayerColor getNextPlayer() {
        return nextPlayer.getColor();
    }
    
    /**
     * Play at the given column.
     * 
     * After this call, if the play was possible, the next player's turn starts and the model is updated.
     * If it is not possible to play here (column is full or wrong index) nothing is done.
     * 
     * @param column Column index (x position) in which to play
     */
    public void play(int column) {
        if (model.play(column, nextPlayer.getColor())) {
            moveList.addMove(new GameMove(nextPlayer.getColor(), column));

            setNextPlayer(nextPlayer == yellowPlayer ? redPlayer : yellowPlayer);
        }
    }
    
    /**
     * Rewind the game to the given move number. If this move number is invalid, nothing is done.
     * 
     * @param moveNumber Index of the last move to keep
     */
    public void rewindToMove(int moveNumber) {
        if (moveNumber >= 0) {
            while (moveList.getSize() > moveNumber + 1) {
                GameMove move = moveList.getAndRemoveLastMove();
                model.remove(move.getColumn());
            }
            setNextPlayer(moveList.getElementAt(moveList.getSize() - 1).getPlayer() == PlayerColor.RED ? yellowPlayer : redPlayer);
        }
    }
    
    /**
     * Set the game panel associated with this controller.
     * 
     * @param gamePanel Game panel to set
     */
    public void setGamePanel(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
    }
    
    /**
     * Return the GameModel of this controller.
     * 
     * @return GameModel of this controller
     */
    public GameModel getModel() {
        return model;
    }
    
    /**
     * Return the GameMoveList of this controller.
     * 
     * @return GameMoveList of this controller
     */
    public GameMoveList getMoveList() {
        return moveList;
    }
    
    /**
     * Tell if the graphic board should be freezed (= non-human player for next turn).
     * 
     * @return True if and only if the graphic board should be freezed
     */
    public boolean isGraphicBoardFreezed() {
        return model.isGameOver() || !nextPlayer.usesGraphicBoard();
    }
    
    /**
     * Helper method: set the next player.
     * 
     * @param nextPlayer Next player
     */
    private void setNextPlayer(Player nextPlayer) {
        this.nextPlayer = nextPlayer;
        
        // run next player's turn if game is not over
        if (!model.isGameOver()) {
            executorService.execute(nextPlayer::run);
        }
        
        // update panel if a graphic component has been set
        if (null != gamePanel) {
            SwingUtilities.invokeLater(() -> gamePanel.update());
        }
    }
}
