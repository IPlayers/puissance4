import static org.junit.jupiter.api.Assertions.*;

import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;
import org.junit.jupiter.api.Test;

import fr.epsi.puissancek.controller.GameController;

class GameModelTest {

	@Test
	void test_play() {
		GameModel Model1 = new GameModel(5,5,3);
		GameModel Model2 = new GameModel(5,5,3);
		boolean result;

		result = Model1.play(2, PlayerColor.RED);
		assertTrue(result);

		result = Model2.play(6, PlayerColor.YELLOW);
		assertFalse(result);
	}

	@Test
	void test_remove() {
		GameModel Model1 = new GameModel(5,5,3);
		PlayerColor result = null;

		Model1.play(1,PlayerColor.RED);
		Model1.remove(1);

		result = Model1.getCell(1,1);
		assertNull(result);
	}

}