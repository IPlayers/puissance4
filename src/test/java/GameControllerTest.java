import fr.epsi.puissancek.controller.GameController;
import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.PlayerColor;
import fr.epsi.puissancek.model.PlayerType;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameControllerTest {

	@Test
	void test_play(){
		GameModel Model1 = new GameModel(5,5,3);
		GameController Controller1 = new GameController(Model1, PlayerType.HUMAN, PlayerType.RANDOM_AI);
		PlayerColor nextPlayer;

		nextPlayer=Controller1.getNextPlayer();

		assertEquals( PlayerColor.RED, nextPlayer);
		assertNotEquals( PlayerColor.YELLOW, nextPlayer);

		Controller1.play(1);
		nextPlayer=Controller1.getNextPlayer();

		assertEquals( PlayerColor.YELLOW, nextPlayer);
		assertNotEquals( PlayerColor.RED, nextPlayer);
	}

	@Test
	void test_rewindToMove(){
		GameModel Model1 = new GameModel(5,5,3);
		GameController Controller1 = new GameController(Model1, PlayerType.HUMAN, PlayerType.RANDOM_AI);


	}

	@Test
	void test_isGraphicBoardFreezed() {
		GameModel Model1 = new GameModel(5,5,3);
		GameController Controller1 = new GameController(Model1, PlayerType.HUMAN, PlayerType.RANDOM_AI);
		boolean result;
		result=Controller1.isGraphicBoardFreezed();
		assertFalse(result);
		Controller1.play(1);
		result=Controller1.isGraphicBoardFreezed();
		assertTrue(result);
	}

}