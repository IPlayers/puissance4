import fr.epsi.puissancek.model.GameModel;
import fr.epsi.puissancek.model.GameMove;
import fr.epsi.puissancek.model.GameMoveList;
import fr.epsi.puissancek.model.PlayerColor;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GameMoveListTest {

	@Test
	void test_getAndRemoveLastMove() {
		GameMoveList list1 = new GameMoveList();
		GameMove move1 = new GameMove(PlayerColor.RED, 1);
		int result;

		result = list1.getSize();
		assertEquals(0, result);

		list1.addMove(move1);
		result = list1.getSize();
		assertEquals(1, result);

		list1.getAndRemoveLastMove();
		result = list1.getSize();
		assertEquals(0, result);
	}

}